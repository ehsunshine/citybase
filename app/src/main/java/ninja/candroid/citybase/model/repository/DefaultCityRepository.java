package ninja.candroid.citybase.model.repository;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ninja.candroid.citybase.entities.City;
import ninja.candroid.citybase.utils.promise.Function;
import ninja.candroid.citybase.utils.promise.Promise;

public class DefaultCityRepository implements CityRepository {

    private Reader reader;
    private List<City> cities;

    public DefaultCityRepository(@NonNull Reader reader) {
        this.reader = reader;
    }

    @NonNull
    public static DefaultCityRepository from(@NonNull Context context) {
        try {
            return from(new InputStreamReader(context.getAssets().open("cities.json"), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot open cities.json file!", e);
        }
    }

    @NonNull
    public static DefaultCityRepository from(@NonNull Reader reader) {
        return new DefaultCityRepository(new BufferedReader(reader));
    }

    @Override
    public List<City> getCities(int fromIndex, int toIndex) {
        return cities.subList(fromIndex, toIndex);
    }

    @Override
    public Promise<List<City>> getCities(Function<City, Boolean> predicate) {
        return Promise.just(cities)
                .map(cities -> {
                    List<City> filteredCities = new ArrayList<>();
                    for (City city : cities) {
                        if (predicate.apply(city)) {
                            filteredCities.add(city);
                        }
                    }
                    return filteredCities;
                });
    }

    @Override
    public Promise<Boolean> prepareCitySource() {
        if (cities == null) {
            return Promise.from(() -> {
                List<City> cities = new ArrayList<>();

                Gson gson = new GsonBuilder().create();
                JsonReader jsonReader = new JsonReader(reader);
                jsonReader.beginArray();

                while (jsonReader.hasNext()) {
                    cities.add(gson.fromJson(jsonReader, City.class));
                }
                reader.close();
                return cities;
            }).map(cities -> {
                Collections.sort(cities, getComparator());
                return cities;
            }).map(cities -> {
                this.cities = cities;
                return cities;
            }).map(cities1 -> true);
        } else {
            return Promise.just(true);
        }
    }

    private Comparator<City> getComparator() {
        return (city1, city2) -> {
            int c1 = city1.getName().compareTo(city2.getName());
            if (c1 != 0) return c1;
            return city1.getCountry().compareTo(city2.getCountry());
        };
    }
}