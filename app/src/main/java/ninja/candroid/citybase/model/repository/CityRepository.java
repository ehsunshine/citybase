package ninja.candroid.citybase.model.repository;

import java.util.List;

import ninja.candroid.citybase.entities.City;
import ninja.candroid.citybase.utils.promise.Function;
import ninja.candroid.citybase.utils.promise.Promise;

public interface CityRepository {
    List<City> getCities(int startIndex, int numberOfItems);

    Promise<List<City>> getCities(Function<City, Boolean> predicate);

    Promise<Boolean> prepareCitySource();
}
