package ninja.candroid.citybase.ui.city.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ninja.candroid.citybase.R;
import ninja.candroid.citybase.entities.City;

public class CityViewHolder extends RecyclerView.ViewHolder {

    private View itemView;
    private City city;
    private CityListener cityListener;

    private CityViewHolder(@NonNull View itemView, @NonNull CityListener cityListener) {
        super(itemView);
        this.itemView = itemView;
        this.cityListener = cityListener;
    }

    public void onBindView(@NonNull City city) {
        this.city = city;

        TextView titleTextView = itemView.findViewById(R.id.title_text_view);
        View cityItem = itemView.findViewById(R.id.city_item);

        titleTextView.setText(String.format("%s, %s",city.getName(),city.getCountry()));
        cityItem.setOnClickListener(v -> onCityClick());
    }

    private void onCityClick() {
        if (cityListener != null) {
            cityListener.onCityClick(city);
        }
    }

    public interface CityListener {
        void onCityClick(@NonNull City city);
    }

    public static class Builder {
        private ViewGroup parent;
        private CityListener cityListener;

        @NonNull
        public Builder parent(@NonNull ViewGroup parent) {
            this.parent = parent;
            return this;
        }

        @NonNull
        public Builder cityListener(@NonNull CityListener cityListener) {
            this.cityListener = cityListener;
            return this;
        }

        @NonNull
        public CityViewHolder build() {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_city_item, parent, false);

            return new CityViewHolder(view, cityListener);
        }
    }
}

