package ninja.candroid.citybase.ui.launch;

import ninja.candroid.citybase.ui.base.BasePresenterContract;
import ninja.candroid.citybase.ui.base.BaseViewContract;

public interface LaunchContract {

    interface View extends BaseViewContract {
        void showHomeScreen();
    }

    interface Presenter extends BasePresenterContract<View> {

    }
}