package ninja.candroid.citybase.ui.launch;

import android.support.annotation.NonNull;

public class LaunchPresenter implements LaunchContract.Presenter {

    private LaunchContract.View view;

    @Override
    public void onBindView(@NonNull LaunchContract.View view) {
        this.view = view;
    }

    @Override
    public void onViewInitialized() {
        view.showHomeScreen();
    }

    @Override
    public void onViewDestroyed() {

    }
}