package ninja.candroid.citybase.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import ninja.candroid.citybase.R;
import ninja.candroid.citybase.ui.base.BaseActivity;
import ninja.candroid.citybase.ui.city.CityFragment;

public class MainActivity extends BaseActivity implements MainContract.View {

    Toolbar toolbar;

    MainContract.Presenter presenter;

    public static Intent newIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void initViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        presenter.onViewInitialized();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.MaterialTheme);
        super.onCreate(savedInstanceState);
        presenter = new MainPresenter();
        presenter.onBindView(this);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showCityFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, CityFragment.newInstance())
                .commit();
    }
}
