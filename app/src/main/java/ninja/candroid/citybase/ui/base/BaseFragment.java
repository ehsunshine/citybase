package ninja.candroid.citybase.ui.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import ninja.candroid.citybase.CityBaseApplication;
import ninja.candroid.citybase.utils.injector.Injector;

public abstract class BaseFragment extends Fragment implements BaseViewContract {

    protected Injector injector;

    protected abstract void injectDependencies();
    protected abstract void initViews();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injector = CityBaseApplication.getInstance().getInjector();
        injectDependencies();
        initViews();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void showErrorMessage(@NonNull String errorMessage) {
        if (getView() == null) {
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
        } else {
            Snackbar.make(getView(), errorMessage, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void showErrorMessage(int errorMessageResourceId) {
        if (getView() == null) {
            Toast.makeText(getActivity(), errorMessageResourceId, Toast.LENGTH_LONG).show();
        } else {
            Snackbar.make(getView(), errorMessageResourceId, Snackbar.LENGTH_LONG).show();
        }
    }

    protected void updateScreenTitle(@Nullable String title) {
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        if (title != null && appCompatActivity != null && appCompatActivity.getSupportActionBar() != null)
            appCompatActivity.getSupportActionBar().setTitle(title);
    }

}
