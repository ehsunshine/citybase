package ninja.candroid.citybase.ui.city;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.concurrent.Executor;

import ninja.candroid.citybase.entities.City;
import ninja.candroid.citybase.model.repository.CityRepository;
import ninja.candroid.citybase.utils.promise.CompositeDisposable;
import ninja.candroid.citybase.utils.promise.Executors;
import ninja.candroid.citybase.utils.promise.Promise;

public class CityPresenter implements CityContract.Presenter {

    private static final int NUMBER_OF_CITIES_IN_A_PAGE = 25;
    private int pageNumber = 0;

    private Executor searchExecutor;

    private CityRepository cityRepository;
    private CityContract.View view;
    private CompositeDisposable disposable = new CompositeDisposable();

    public CityPresenter(@NonNull CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public void onBindView(@NonNull CityContract.View view) {
        this.view = view;
    }

    @Override
    public void onViewInitialized() {
        view.showLoading();
        disposable.add(cityRepository.prepareCitySource()
                .execute(result -> {
                    loadCities(pageNumber);
                    view.hideLoading();
                }, error -> {
                    view.showErrorMessage(error.getLocalizedMessage());
                    view.hideLoading();
                }));
    }

    @Override
    public void onViewDestroyed() {
        disposable.dispose();
    }

    @Override
    public void onCityClicked(@NonNull City city) {
        view.showCityOnTheMap(city);
    }

    @Override
    public void onLoadMoreClicked() {
        loadCities(++pageNumber);
    }

    @Override
    public void onSearchRequested(String prefix) {
        if (prefix.isEmpty()) {
            view.showCities(Collections.emptyList(), true);
            pageNumber = 0;
            loadCities(pageNumber);
            return;
        }
        if (searchExecutor == null) {
            searchExecutor = Executors.newThread();
        }
        disposable.add(cityRepository.getCities(city -> city.getName().toLowerCase().startsWith(prefix.toLowerCase()))
                .executeOn(searchExecutor)
                .execute(cities -> {
                    if (cities.size() == 0) {
                        view.showEmptyList();
                    } else {
                        view.hideEmptyList();
                    }
                    view.showCities(cities, true);
                }, error -> {
                    view.showErrorMessage(error.getLocalizedMessage());
                }));
    }

    private void loadCities(int startIndex) {
        int fromIndex = startIndex * NUMBER_OF_CITIES_IN_A_PAGE;
        int toIndex = (fromIndex + NUMBER_OF_CITIES_IN_A_PAGE) - 1;

        disposable.add(Promise.from(() -> cityRepository.getCities(fromIndex, toIndex))
                .execute(cities -> {
                            if (cities.size() == 0) {
                                view.showEmptyList();
                            } else {
                                view.hideEmptyList();
                            }
                            view.showCities(cities, false);
                        },
                        throwable -> {
                            view.showEmptyList();
                            view.showErrorMessage(throwable.getLocalizedMessage());
                        }));
    }
}
