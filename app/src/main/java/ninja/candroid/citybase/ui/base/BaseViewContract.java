package ninja.candroid.citybase.ui.base;

import android.support.annotation.NonNull;

public interface BaseViewContract {

    void showErrorMessage(@NonNull String errorMessage);

    void showErrorMessage(int errorMessageResourceId);

}
