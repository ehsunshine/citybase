package ninja.candroid.citybase.ui.main;

import ninja.candroid.citybase.ui.base.BasePresenterContract;
import ninja.candroid.citybase.ui.base.BaseViewContract;

public interface MainContract {
    interface View extends BaseViewContract {
        void showCityFragment();
    }

    interface Presenter extends BasePresenterContract<MainContract.View> {

    }
}
