package ninja.candroid.citybase.ui.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import ninja.candroid.citybase.CityBaseApplication;
import ninja.candroid.citybase.utils.injector.Injector;

public abstract class BaseActivity extends AppCompatActivity implements BaseViewContract {

    protected Injector injector;
    protected abstract void initViews();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injector = CityBaseApplication.getInstance().getInjector();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        initViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void updateScreenTitle(@Nullable String title) {
        if (title != null && getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void showErrorMessage(@NonNull String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorMessage(int errorMessageResourceId) {
        Toast.makeText(this, errorMessageResourceId, Toast.LENGTH_LONG).show();
    }

}