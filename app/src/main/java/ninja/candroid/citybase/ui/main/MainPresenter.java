package ninja.candroid.citybase.ui.main;

import android.support.annotation.NonNull;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;

    @Override
    public void onBindView(@NonNull MainContract.View view) {
        this.view = view;
    }

    @Override
    public void onViewInitialized() {
        view.showCityFragment();
    }

    @Override
    public void onViewDestroyed() {

    }
}
