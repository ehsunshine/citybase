package ninja.candroid.citybase.ui.launch;

import android.os.Bundle;
import android.support.annotation.Nullable;

import ninja.candroid.citybase.R;
import ninja.candroid.citybase.ui.base.BaseActivity;
import ninja.candroid.citybase.ui.main.MainActivity;

public class LaunchActivity extends BaseActivity implements LaunchContract.View {

    LaunchContract.Presenter presenter;

    @Override
    protected void initViews() {
        presenter.onViewInitialized();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new LaunchPresenter();
        presenter.onBindView(this);
        setContentView(R.layout.activity_launcher);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }

    @Override
    public void showHomeScreen() {
        startActivity(MainActivity.newIntent(this));
        finish();
    }
}