package ninja.candroid.citybase.ui.city.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ninja.candroid.citybase.entities.City;
import ninja.candroid.citybase.ui.city.viewholder.CityViewHolder;
import ninja.candroid.citybase.ui.city.viewholder.LoadMoreViewHolder;

public class CityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        CityViewHolder.CityListener {

    private static final int TYPE_CITY = 1;
    private static final int TYPE_LOAD_MORE = 2;

    private boolean shouldDisplayLoadMore = true;

    private List<City> cityList = new ArrayList<>();
    private CityAdapter.CityEventListener cityEventListener;

    public CityAdapter(@NonNull CityEventListener cityEventListener) {
        this.cityEventListener = cityEventListener;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_CITY:
                return new CityViewHolder.Builder()
                        .parent(parent)
                        .cityListener(city -> cityEventListener.onCityClick(city))
                        .build();

            case TYPE_LOAD_MORE:
                return new LoadMoreViewHolder.Builder()
                        .parent(parent)
                        .loadMoreListener(() -> cityEventListener.onLoadMoreClick())
                        .build();
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CityViewHolder) {
            City city = cityList.get(position);
            ((CityViewHolder) holder).onBindView(city);
        } else if (holder instanceof LoadMoreViewHolder) {
            ((LoadMoreViewHolder) holder).onBindView();
        }

    }

    @Override
    public int getItemCount() {
        return cityList.size() + (shouldDisplayLoadMore && cityList.size() > 0 ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return position == cityList.size()
                ? TYPE_LOAD_MORE
                : TYPE_CITY;
    }

    public void setShouldDisplayLoadMore(boolean shouldDisplayLoadMore) {
        this.shouldDisplayLoadMore = shouldDisplayLoadMore;
        notifyDataSetChanged();
    }


    public void addCities(@NonNull List<City> cityList) {
        this.cityList.addAll(cityList);
        notifyDataSetChanged();
    }

    public void setCities(@NonNull List<City> cityList) {
        this.cityList.clear();
        this.cityList.addAll(cityList);
        notifyDataSetChanged();
    }

    @Override
    public void onCityClick(@NonNull City city) {
        cityEventListener.onCityClick(city);
    }

    public interface CityEventListener {
        void onCityClick(@NonNull City city);

        void onLoadMoreClick();
    }
}

