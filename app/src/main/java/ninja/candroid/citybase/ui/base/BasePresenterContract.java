package ninja.candroid.citybase.ui.base;

import android.support.annotation.NonNull;

public interface BasePresenterContract<T extends BaseViewContract> {
    void onBindView(@NonNull T view);

    void onViewInitialized();

    void onViewDestroyed();
}