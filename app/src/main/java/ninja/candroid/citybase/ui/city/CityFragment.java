package ninja.candroid.citybase.ui.city;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import ninja.candroid.citybase.R;
import ninja.candroid.citybase.entities.City;
import ninja.candroid.citybase.ui.base.BaseFragment;
import ninja.candroid.citybase.ui.city.adapter.CityAdapter;

public class CityFragment extends BaseFragment implements CityContract.View,
        CityAdapter.CityEventListener {

    CityContract.Presenter presenter;
    private CityAdapter adapter;
    private View loadingView;
    private TextView emptyListView;

    public static CityFragment newInstance() {
        return new CityFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_city, container, false);
    }

    @Override
    public void showCities(List<City> cities, boolean searchView) {
        if (searchView) {
            adapter.setShouldDisplayLoadMore(false);
            adapter.setCities(cities);
        } else {
            adapter.setShouldDisplayLoadMore(true);
            adapter.addCities(cities);
        }
    }

    @Override
    public void showCityOnTheMap(@NonNull City city) {
        double latitude = city.getCoord().getLat();
        double longitude = city.getCoord().getLon();
        String label = city.getName();
        String uriBegin = "geo:" + latitude + "," + longitude;
        String query = latitude + "," + longitude + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyList() {
        emptyListView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyList() {
        emptyListView.setVisibility(View.GONE);
    }

    @Override
    protected void injectDependencies() {
        presenter = injector.provideCityPresenter();
        presenter.onBindView(this);
    }

    @Override
    protected void initViews() {
        RecyclerView cityRecyclerView = getActivity().findViewById(R.id.recycler_view);
        EditText filterEditText = getActivity().findViewById(R.id.filter_text_view);
        loadingView = getActivity().findViewById(R.id.loading_container);
        emptyListView = getActivity().findViewById(R.id.empty_list_view);
        adapter = new CityAdapter(this);
        cityRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        cityRecyclerView.setAdapter(adapter);

        filterEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchCities(s.toString());
            }
        });

        presenter.onViewInitialized();
    }

    @Override
    public void onCityClick(@NonNull City city) {
        presenter.onCityClicked(city);

    }

    @Override
    public void onLoadMoreClick() {
        presenter.onLoadMoreClicked();
    }

    private void searchCities(String prefix) {

        presenter.onSearchRequested(prefix);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onViewDestroyed();
    }
}