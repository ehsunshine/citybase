package ninja.candroid.citybase.ui.city;

import android.support.annotation.NonNull;

import java.util.List;

import ninja.candroid.citybase.entities.City;
import ninja.candroid.citybase.ui.base.BasePresenterContract;
import ninja.candroid.citybase.ui.base.BaseViewContract;

public interface CityContract {

    interface View extends BaseViewContract {
        void showCities(List<City> cities, boolean searchView);
        void showCityOnTheMap(@NonNull City city);
        void showLoading();
        void hideLoading();
        void showEmptyList();
        void hideEmptyList();
    }

    interface Presenter extends BasePresenterContract<View> {
        void onCityClicked(@NonNull City city);
        void onLoadMoreClicked();

        void onSearchRequested(String prefix);
    }
}