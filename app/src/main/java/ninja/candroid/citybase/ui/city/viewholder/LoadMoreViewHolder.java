package ninja.candroid.citybase.ui.city.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ninja.candroid.citybase.R;

public class LoadMoreViewHolder extends RecyclerView.ViewHolder {

    private LoadMoreListener loadMoreListener;
    private View itemView;

    private LoadMoreViewHolder(@NonNull View itemView, @NonNull LoadMoreListener loadMoreListener) {
        super(itemView);
        this.itemView = itemView;
        this.loadMoreListener = loadMoreListener;
    }

    public void onBindView() {
        View loadMore = itemView.findViewById(R.id.root_container);
        loadMore.setOnClickListener(v -> onLoadMoreClick());
    }

    void onLoadMoreClick() {
        if (loadMoreListener != null) {
            loadMoreListener.onLoadMoreClick();
        }
    }

    public interface LoadMoreListener {
        void onLoadMoreClick();
    }

    public static class Builder {
        private ViewGroup parent;
        private LoadMoreListener loadMoreListener;

        @NonNull
        public Builder parent(@NonNull ViewGroup parent) {
            this.parent = parent;
            return this;
        }

        @NonNull
        public Builder loadMoreListener(@NonNull LoadMoreListener loadMoreListener) {
            this.loadMoreListener = loadMoreListener;
            return this;
        }

        @NonNull
        public LoadMoreViewHolder build() {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_load_more, parent, false);

            return new LoadMoreViewHolder(view, loadMoreListener);
        }
    }
}

