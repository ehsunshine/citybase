package ninja.candroid.citybase;

import android.app.Application;

import ninja.candroid.citybase.utils.injector.DefaultInjector;
import ninja.candroid.citybase.utils.injector.Injector;

public class CityBaseApplication extends Application {

    private static CityBaseApplication instance;
    private Injector injector;

    public static CityBaseApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        injector = new DefaultInjector(this);
    }

    public Injector getInjector() {
        return injector;
    }
}