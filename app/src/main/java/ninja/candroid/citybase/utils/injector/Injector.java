package ninja.candroid.citybase.utils.injector;

import ninja.candroid.citybase.model.repository.CityRepository;
import ninja.candroid.citybase.ui.city.CityContract;

public interface Injector {

    CityRepository provideCityRepository();

    CityContract.Presenter provideCityPresenter();
}