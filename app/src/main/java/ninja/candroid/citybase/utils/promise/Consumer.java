package ninja.candroid.citybase.utils.promise;

public interface Consumer<T> {
    void consume(T t);
}
