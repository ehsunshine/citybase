package ninja.candroid.citybase.utils.injector;

import android.content.Context;

import ninja.candroid.citybase.model.repository.CityRepository;
import ninja.candroid.citybase.model.repository.DefaultCityRepository;
import ninja.candroid.citybase.ui.city.CityContract;
import ninja.candroid.citybase.ui.city.CityPresenter;

public class DefaultInjector implements Injector {

    private Context context;
    private CityRepository cityRepository;

    public DefaultInjector(Context context) {
        this.context = context;
    }

    @Override
    public CityRepository provideCityRepository() {
        if (cityRepository == null) {
            cityRepository = DefaultCityRepository.from(context);
        }
        return cityRepository;
    }

    @Override
    public CityContract.Presenter provideCityPresenter() {
        return new CityPresenter(provideCityRepository());
    }
}