package ninja.candroid.citybase.utils.promise;

public interface Function<T, R> {
    R apply(T t);
}
