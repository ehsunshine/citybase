package ninja.candroid.citybase.utils.promise;

public interface Disposable {
    void dispose();
}

