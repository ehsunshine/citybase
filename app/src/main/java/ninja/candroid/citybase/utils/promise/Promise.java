package ninja.candroid.citybase.utils.promise;


import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

public class Promise<T> {

    private Callable<T> callable;
    private CompositeConsumer<T> doOnSuccess = new CompositeConsumer<>();
    private CompositeConsumer<Throwable> doOnError = new CompositeConsumer<>();
    private Executor executionExecutor;

    private Promise(@NonNull Callable<T> callable) {
        this.callable = callable;
    }

    @NonNull
    public static <V> Promise<V> from(@NonNull Callable<V> callable) {
        return new Promise<>(callable);
    }

    @NonNull
    public static <V> Promise<V> just(@NonNull V value) {
        return new Promise<>(() -> value);
    }

    @NonNull
    public <R> Promise<R> map(@NonNull Function<T, R> func) {
        return Promise.from(() -> func.apply(callable.call()));
    }

    @NonNull
    public Promise<T> doOnSuccess(@NonNull Consumer<T> consumer) {
        this.doOnSuccess.addConsumer(consumer);
        return this;
    }

    @NonNull
    public Promise<T> doOnError(@NonNull Consumer<Throwable> consumer) {
        this.doOnError.addConsumer(consumer);
        return this;
    }

    @NonNull
    public Promise<T> executeOn(@NonNull Executor executor) {
        this.executionExecutor = executor;
        return this;
    }

    @NonNull
    public Disposable execute(@NonNull Consumer<T> onSuccess,
                              @NonNull Consumer<Throwable> onError) {

        Consumer<T> successConsumer = CompositeConsumer.from(doOnSuccess).addConsumer(onSuccess);
        Consumer<Throwable> errorConsumer = CompositeConsumer.from(doOnError).addConsumer(onError);
        Executor executor = executionExecutor != null ? executionExecutor : AsyncTask.THREAD_POOL_EXECUTOR;

        PromiseAsyncTask<T> promiseAsyncTask = new PromiseAsyncTask<>(callable, successConsumer, errorConsumer);
        promiseAsyncTask.executeOnExecutor(executor);
        return promiseAsyncTask;
    }

    private static class PromiseAsyncTask<V> extends AsyncTask<Void, Void, V> implements Disposable {

        private Callable<V> callable;
        private Consumer<V> onSuccess;
        private Consumer<Throwable> onError;
        private Throwable thrownException;

        public PromiseAsyncTask(@NonNull Callable<V> callable,
                                @NonNull Consumer<V> onSuccess,
                                @Nullable Consumer<Throwable> onError) {
            this.callable = callable;
            this.onSuccess = onSuccess;
            this.onError = onError;
        }

        @Override
        protected V doInBackground(Void... voids) {
            try {
                return callable.call();
            } catch (Exception e) {
                thrownException = e;
                return null;
            }
        }

        @Override
        protected void onPostExecute(V v) {
            if (v == null) {
                if (onError != null) {
                    onError.consume(thrownException);
                }
            } else {
                if (onSuccess != null) {
                    onSuccess.consume(v);
                }
            }
        }

        @Override
        public void dispose() {
            onSuccess = null;
            onError = null;
            cancel(true);
        }
    }
}