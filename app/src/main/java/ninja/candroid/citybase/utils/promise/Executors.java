package ninja.candroid.citybase.utils.promise;

import android.os.HandlerThread;
import android.os.Looper;

import java.util.UUID;
import java.util.concurrent.Executor;

public class Executors {

    public static Executor newThread() {
        HandlerThread handlerThread = new HandlerThread(UUID.randomUUID().toString());
        handlerThread.start();
        return new LooperExecutor(handlerThread.getLooper());
    }

    public static Executor mainThread() {
        return new LooperExecutor(Looper.getMainLooper());
    }
}