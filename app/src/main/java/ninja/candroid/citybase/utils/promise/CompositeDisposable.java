package ninja.candroid.citybase.utils.promise;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CompositeDisposable implements Disposable {

    private List<Disposable> disposables;

    public CompositeDisposable() {
        this.disposables = new ArrayList<>();
    }

    public CompositeDisposable(@NonNull Disposable... disposables) {
        this.disposables = new ArrayList<>();
        Collections.addAll(this.disposables, disposables);
    }

    @Override
    public void dispose() {
        clear(this.disposables);
        this.disposables = null;
    }

    public boolean add(@NonNull Disposable disposable) {
        return this.disposables.add(disposable);
    }

    private void clear(List<Disposable> disposables) {
        if (disposables == null) {
            return;
        }

        for (Disposable disposable : disposables) {
            disposable.dispose();
        }
    }

}
