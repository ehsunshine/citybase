package ninja.candroid.citybase.utils.promise;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

public class LooperExecutor implements Executor {

    private final Handler handler;

    public LooperExecutor(@NonNull Looper looper) {
        this.handler = new Handler(looper);
    }

    @Override
    public void execute(@NonNull Runnable command) {
        handler.post(command);
    }
}