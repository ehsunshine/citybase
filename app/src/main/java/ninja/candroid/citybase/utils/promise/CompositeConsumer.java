package ninja.candroid.citybase.utils.promise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CompositeConsumer<T> implements Consumer<T> {

    private List<Consumer<T>> consumers = new ArrayList<>();

    public CompositeConsumer() {
    }

    public static <V> CompositeConsumer<V> from(Consumer<V>... consumers) {
        CompositeConsumer<V> compositeConsumer = new CompositeConsumer<>();
        compositeConsumer.consumers.addAll(Arrays.asList(consumers));
        return compositeConsumer;
    }

    public static <V> CompositeConsumer<V> from(CompositeConsumer<V> compositeConsumer) {
        CompositeConsumer<V> newCompositeConsumer = new CompositeConsumer<>();
        newCompositeConsumer.consumers.addAll(compositeConsumer.consumers);
        return newCompositeConsumer;
    }

    public CompositeConsumer<T> addConsumer(Consumer<T> consumer) {
        this.consumers.add(consumer);
        return this;
    }

    @Override
    public void consume(T t) {
        for (Consumer<T> consumer : consumers) {
            consumer.consume(t);
        }
    }
}