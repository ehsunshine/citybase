package ninja.candroid.citybase.model.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import ninja.candroid.citybase.entities.City;
import ninja.candroid.citybase.shadow.MyShadowAsyncTask;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(shadows = {MyShadowAsyncTask.class})
public class DefaultCityRepositoryTest {

    private DefaultCityRepository cityRepository;

    @Before
    public void setUp() {
        cityRepository = DefaultCityRepository.from(new InputStreamReader(getCitiesInputStream()));
        cityRepository.prepareCitySource()
                .execute(result -> {
                            //Do nothing!
                        },
                        error -> {
                            //Do nothing!
                        });

        Robolectric.flushBackgroundThreadScheduler();
        Robolectric.flushForegroundThreadScheduler();
    }

    @Test
    public void shouldFindCitiesForCorrectInputs() {
        final String INPUT_PREFIX = "W";
        TestConsumer<List<City>> cityListConsumer = new TestConsumer<>();
        TestConsumer<Throwable> errorConsumer = new TestConsumer<>();
        cityRepository.getCities(city -> city.getName().startsWith(INPUT_PREFIX))
                .execute(cityListConsumer, errorConsumer);

        Robolectric.flushBackgroundThreadScheduler();
        Robolectric.flushForegroundThreadScheduler();

        assertEquals(0, errorConsumer.getConsumedItems().size());
        assertEquals(1, cityListConsumer.getConsumedItems().size());
        List<City> resultCities = cityListConsumer.getConsumedItems().get(0);
        assertEquals(2, resultCities.size());
        //The rest of entity checking can be here!
    }

    @Test
    public void shouldFindNothingForIncorrectInputs() {
        final String INPUT_PREFIX = "X";

        TestConsumer<List<City>> cityListConsumer = new TestConsumer<>();
        TestConsumer<Throwable> errorConsumer = new TestConsumer<>();
        cityRepository.getCities(city -> city.getName().startsWith(INPUT_PREFIX))
                .execute(cityListConsumer, errorConsumer);

        Robolectric.flushBackgroundThreadScheduler();
        Robolectric.flushForegroundThreadScheduler();

        assertEquals(0, errorConsumer.getConsumedItems().size());
        assertEquals(1, cityListConsumer.getConsumedItems().size());
        List<City> resultCities = cityListConsumer.getConsumedItems().get(0);
        assertEquals(0, resultCities.size());
    }

    private InputStream getCitiesInputStream() {
        String citiesString = "[{\"country\":\"BE\",\"name\":\"Ieper\",\"_id\":2795101,\"coord\":{\"lon\":2.86733,\"lat\":50.85704}},\n" +
                "{\"country\":\"BF\",\"name\":\"Titao\",\"_id\":2354349,\"coord\":{\"lon\":-2.06667,\"lat\":13.76667}},\n" +
                "{\"country\":\"BF\",\"name\":\"Cascades\",\"_id\":6930703,\"coord\":{\"lon\":-4.76292,\"lat\":10.65015}},\n" +
                "{\"country\":\"BO\",\"name\":\"Villazon\",\"_id\":3901501,\"coord\":{\"lon\":-65.594223,\"lat\":-22.08659}},\n" +
                "{\"country\":\"CA\",\"name\":\"Laval\",\"_id\":6050612,\"coord\":{\"lon\":-73.749184,\"lat\":45.583382}},\n" +
                "{\"country\":\"CD\",\"name\":\"Yangambi\",\"_id\":203717,\"coord\":{\"lon\":24.43359,\"lat\":0.81021}},\n" +
                "{\"country\":\"CD\",\"name\":\"Watsa\",\"_id\":204283,\"coord\":{\"lon\":29.535509,\"lat\":3.03716}},\n" +
                "{\"country\":\"CD\",\"name\":\"Wamba\",\"_id\":204318,\"coord\":{\"lon\":27.994659,\"lat\":2.14838}},\n" +
                "{\"country\":\"CD\",\"name\":\"Lusambo\",\"_id\":210379,\"coord\":{\"lon\":23.450001,\"lat\":-4.96667}},\n" +
                "{\"country\":\"CD\",\"name\":\"Luebo\",\"_id\":210939,\"coord\":{\"lon\":21.41667,\"lat\":-5.35}}]";

        return new ByteArrayInputStream(citiesString.getBytes(StandardCharsets.UTF_8));
    }
}