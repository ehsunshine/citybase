package ninja.candroid.citybase.model.repository;

import java.util.ArrayList;
import java.util.List;

import ninja.candroid.citybase.utils.promise.Consumer;

public class TestConsumer<T> implements Consumer<T> {

    private List<T> consumedItems = new ArrayList<>();

    @Override
    public void consume(T t) {
        consumedItems.add(t);
    }

    public List<T> getConsumedItems() {
        return new ArrayList<>(consumedItems);
    }
}
